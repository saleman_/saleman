﻿namespace Saleman.Ecommerce.Initialization
{
    using Saleman.Ecommerce.Payment;
    using Saleman.Ecommerce.Payment.Onepay;
    using WebFramework.Infrastructure.Attributes;
    using WebFramework.Infrastructure.Initialization;

    [InitializableModule]
    public class InitializationModule : IInitializableModule
    {
        public void Initialize(InitializationContext context)
        {
            context.Services.AddSingleton<IPaymentProcessor, OnepayProcessor>(PaymentKeys.OnepayProvider);
        }

        public void UnInitialize(InitializationContext context)
        {
        }
    }
}