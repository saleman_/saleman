﻿namespace Saleman.Ecommerce.Payment.Configurations
{
    public class PaymentProviderConfiguration
    {
        public string ProviderName { get; set; }
        public string Type { get; set; }
    }

    public class PaymentOptions
    {
        public string ProviderName { get; set; }

        public string ProviderUrl { get; set; }

        public string ReturnUrl { get; set; }

        public string Salt { get; set; }
    }

    public class OnePayOptions : PaymentOptions
    {
        public string Version { get; set; }

        public string Command { get; set; }

        public string AccessCode { get; set; }

        public string Merchant { get; set; }
    }
}
